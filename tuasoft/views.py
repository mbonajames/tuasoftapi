import datetime

from django.shortcuts import render
from .models import Company, Accomodation, Destination, Contact, Itinerary, Country, Service, Day, DailyAccomodation, DailyDestination, Includes, Excludes
from .serializers import CompanySerializer, AccomodationSerializer, DestinationSerializer, ContactSerializer, ItinerarySerializer, CountrySerializer, ServiceSerializer, DaySerializer, DailyAccomodationSerializer, DailyDestinationSerializer, IncludesSerializer, ExcludesSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django.utils.dateparse import parse_date

class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

class AccomodationViewSet(viewsets.ModelViewSet):
    queryset = Accomodation.objects.all()
    serializer_class = AccomodationSerializer

class DestinationViewSet(viewsets.ModelViewSet):
    queryset = Destination.objects.all()
    serializer_class = DestinationSerializer



@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")

    context = {
        "request": request
    }

    company = Company.objects.filter(username=username, password=password).first()
    if company:
        permisson = company.serializable_value('permission')
        if permisson == 'allowed':
            company_data = CompanySerializer(company, context=context).data
            return Response(company_data)
        else:
            return Response({"detail": "Not allowed"}, status=403)
    else:
        return Response({"detail": "Invalid user"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_itinerary(request):
    print(request.data)
    itinerary = ItinerarySerializer(data=request.data)

    if itinerary.is_valid():
        itinerary.save()

        return Response(itinerary.data, status=200)
    else:
        return Response({"detail": "Itinerary not added"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_itinerary(request):

    itinerary = Itinerary.objects.first()
    return Response(itinerary, status=200)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_contact(request):
    company_id = request.query_params.get("id")
    context = {
        "request": request
    }
    contacts = Contact.objects.filter(company=company_id)
    contact_data = ContactSerializer(contacts, many=True, context=context).data

    if contact_data:
        return Response(contact_data, status=200)
    else:
        return Response({"detail": "Contact does not exist"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_include(request):
    include = IncludesSerializer(data=request.data)

    if include.is_valid():
        include.save()

        return Response({"detail": "Include added"}, status=200)
    else:
        return Response({"detail": "Include not added"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_exclude(request):
    exclude = ExcludesSerializer(data=request.data)

    if exclude.is_valid():
        exclude.save()

        return Response({"detail": "Exclude added"}, status=200)
    else:
        return Response({"detail": "Exclude not added"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_day(request):
    day = DaySerializer(data=request.data)

    if day.is_valid():
        day.save()

        return Response(day.data, status=200)
    else:
        return Response({"detail": "Day not added"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_daily_accomodation(request):
    accomodation = DailyAccomodationSerializer(data=request.data)

    if accomodation.is_valid():
        accomodation.save()

        return Response({"detail": "Accomodation not added"}, status=200)
    else:
        return Response({"detail": "Accomodation not added"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_daily_destination(request):
    destination = DailyDestinationSerializer(data=request.data)

    if destination.is_valid():
        destination.save()

        return Response({"detail": "Destination not added"}, status=200)
    else:
        return Response({"detail": "Destination not added"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_itinerary(request):
    itinerary_id = request.query_params.get("id")
    context = {
        "request": request
    }
    subtotal_cost = 0
    itinerary = Itinerary.objects.filter(id=itinerary_id).first()
    itinerary_data = ItinerarySerializer(itinerary, context=context).data
    data = []
    ac_pictures = []
    des_pictures = []
    costs = []
    if itinerary_data:

        days = Day.objects.filter(itinerary=itinerary.serializable_value('id'))
        for day in days:
            day_data = DaySerializer(day, context=context).data
            accomodations = DailyAccomodation.objects.filter(day=day.serializable_value('id'))
            accomodation_data = DailyAccomodationSerializer(accomodations, many=True, context=context).data
            for accomodation in accomodations:
                hotel = Accomodation.objects.get(name=accomodation.serializable_value('name'))
                hotel_data = AccomodationSerializer(hotel, context=context).data
                ac_pictures.append(hotel_data)

                service = Service.objects.get(id=accomodation.serializable_value('service'))
                costs.append(service.serializable_value('price'))

            destinations = DailyDestination.objects.filter(day=day.serializable_value('id'))
            destination_data = DailyDestinationSerializer(destinations, many=True, context=context).data
            for destination in destinations:
                place = Destination.objects.get(name=destination.serializable_value('name'))
                place_data = DestinationSerializer(place, context=context).data
                des_pictures.append(place_data)

                service = Service.objects.get(id=destination.serializable_value('service'))
                costs.append(service.serializable_value('price'))

            day_data["accomodations"] = accomodation_data
            day_data["destinations"] = destination_data
            day_data["acpic"] = ac_pictures
            day_data["despic"] = des_pictures
            data.append(day_data)

        itinerary_data["days"] = data

        includes = Includes.objects.filter(itinerary=itinerary.serializable_value('id'))
        include_data = IncludesSerializer(includes, many=True, context=context).data
        for include in includes:
            service = Service.objects.get(id=include.serializable_value('service'))
            costs.append(service.serializable_value('price'))

        itinerary_data["includes"] = include_data

        exclude = Excludes.objects.filter(itinerary=itinerary.serializable_value('id'))
        exclude_data = ExcludesSerializer(exclude, many=True, context=context).data
        itinerary_data["excludes"] = exclude_data


        for cost in costs:
            subtotal_cost = subtotal_cost + int(cost)
        mark_up = (itinerary.serializable_value('markup') / 100) * subtotal_cost
        total_cost = subtotal_cost + mark_up

        itinerary_data["it_cost"] = subtotal_cost
        itinerary_data["markup_cost"] = mark_up
        itinerary_data["total"] = total_cost

        company = Company.objects.get(id=itinerary.serializable_value('company'))
        company_data = CompanySerializer(company, context=context).data
        itinerary_data["host"] = company_data

        contact = Contact.objects.get(id=itinerary.serializable_value('contact'))
        contact_data = ContactSerializer(contact, context=context).data
        itinerary_data["host_contact"] = contact_data

        country = Country.objects.get(id=itinerary.serializable_value('country'))
        country_data = CountrySerializer(country, context=context).data
        itinerary_data["host_country"] = country_data

        return Response(itinerary_data, status=200)
    else:
        return Response({"detail": "Data not found"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_company(request):
    company_id = request.query_params.get("id")
    contact_id = request.query_params.get("contact")
    context = {
        "request": request
    }
    company = Company.objects.get(id=company_id)
    company_data = CompanySerializer(company, context=context).data

    contacts = Contact.objects.get(id=contact_id)
    contact_data = ContactSerializer(contacts, context=context).data

    company_data["contact"] = contact_data

    if company_data:
        return Response(company_data, status=200)
    else:
        return Response({"detail": "Company does not exist"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_country(request):
    country_id = request.query_params.get("id")

    context = {
        "request": request
    }
    country = Country.objects.get(id=country_id)
    country_data = CountrySerializer(country, context=context).data

    if country_data:
        return Response(country_data, status=200)
    else:
        return Response({"detail": "Country does not exist"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_itineraries(request):
    company_id = request.query_params.get("id")

    context = {
        "request": request
    }
    itinerary = Itinerary.objects.filter(company=company_id).order_by('-created_at')
    itinerary_data = ItinerarySerializer(itinerary, many=True, context=context).data

    if itinerary_data:
        return Response(itinerary_data, status=200)
    else:
        return Response({"detail": "Country does not exist"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_accomodation(request, pk):
    accomodation = DailyAccomodation.objects.filter(id=pk).first()
    accomodation_data = DailyAccomodationSerializer(instance=accomodation, data=request.data)

    if accomodation_data.is_valid():
        accomodation_data.save()
        return Response({"detail": "data updated"}, status=200)
    return Response({"detail": "data not updated"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_Destination(request, pk):
    destination = DailyDestination.objects.filter(id=pk).first()
    destination_data = DailyDestinationSerializer(instance=destination, data=request.data)

    if destination_data.is_valid():
        destination_data.save()
        return Response({"detail": "data updated"}, status=200)
    return Response({"detail": "data not updated"}, status=400)

@api_view(['DELETE'])
# @permission_classes([IsAuthenticated])
def delete(request):
    item_id = request.query_params.get("id")
    item_type = request.query_params.get("type")
    if item_type == "accomodation":
        accomodation = DailyAccomodation.objects.filter(id=item_id).first()
        accomodation.delete()
        return Response({"detail": "Accomodation deleted"}, status=200)

    if item_type == "destination":
        destination = DailyDestination.objects.filter(id=item_id).first()
        destination.delete()
        return Response({"detail": "Destination deleted"}, status=200)

    if item_type == "include":
        include = Includes.objects.filter(id=item_id).first()
        include.delete()
        return Response({"detail": "Include deleted"}, status=200)

    if item_type == "exclude":
        exclude = Excludes.objects.filter(id=item_id).first()
        exclude.delete()
        return Response({"detail": "Exclude deleted"}, status=200)

    if item_type == "itinerary":
        itinerary = Itinerary.objects.filter(id=item_id).first()
        itinerary.delete()
        return Response({"detail": "Itinerary deleted"}, status=200)

    if item_type == "contact":
        contact = Contact.objects.filter(id=item_id).first()
        contact.delete()
        return Response({"detail": "Contact deleted"}, status=200)

    if item_type == "service":
        service = Service.objects.filter(id=item_id).first()
        service.delete()
        return Response({"detail": "Service deleted"}, status=200)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def signup(request):
    company = CompanySerializer(data=request.data)

    if company.is_valid():
        company.save()

        return Response({"detail": "Company added"}, status=200)
    else:
        return Response({"detail": "Company not added"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_company(request, pk):
    company = Company.objects.filter(id=pk).first()
    company_data = CompanySerializer(instance=company, data=request.data)

    if company_data.is_valid():
        company_data.save()
        return Response({"detail": "Company updated"}, status=200)
    return Response({"detail": "Company not updated"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_contact(request):
    contact = ContactSerializer(data=request.data)

    if contact.is_valid():
        contact.save()

        return Response({"detail": "Contact added"}, status=200)
    else:
        return Response({"detail": "Contact not added"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_contact(request, pk):
    contact = Contact.objects.filter(id=pk).first()
    contact_data = ContactSerializer(instance=contact, data=request.data)

    if contact_data.is_valid():
        contact_data.save()
        return Response({"detail": "Contact updated"}, status=200)
    return Response({"detail": "Contact not updated"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def add_service(request):
    service = ServiceSerializer(data=request.data)

    if service.is_valid():
        service.save()

        return Response({"detail": "Service added"}, status=200)
    else:
        return Response({"detail": "Service not added"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_service(request, pk):
    service = Service.objects.filter(id=pk).first()
    service_data = ServiceSerializer(instance=service, data=request.data)

    if service_data.is_valid():
        service_data.save()
        return Response({"detail": "Service updated"}, status=200)
    return Response({"detail": "Service not updated"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_company(request):
    company_id = request.query_params.get("id")

    context = {
        "request": request
    }
    company = Company.objects.get(id=company_id)
    company_data = CompanySerializer(company, context=context).data

    if company_data:
        return Response(company_data, status=200)
    else:
        return Response({"detail": "Company does not exist"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_services(request):
    company_id = request.query_params.get("id")

    context = {
        "request": request
    }
    service = Service.objects.filter(company=company_id)
    services = ServiceSerializer(service, many=True, context=context).data

    if services:
        return Response(services, status=200)
    else:
        return Response({"detail": "No services"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_includes(request):
    itinerary = request.query_params.get("id")

    context = {
        "request": request
    }
    include = Includes.objects.filter(itinerary=itinerary)
    includes = IncludesSerializer(include, many=True, context=context).data

    if includes:
        return Response(includes, status=200)
    else:
        return Response({"detail": "No services"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_excludes(request):
    itinerary = request.query_params.get("id")

    context = {
        "request": request
    }
    exclude = Excludes.objects.filter(itinerary=itinerary)
    excludes = ExcludesSerializer(exclude, many=True, context=context).data

    if excludes:
        return Response(excludes, status=200)
    else:
        return Response({"detail": "No services"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_dailyaccomodations(request):
    day = request.query_params.get("id")

    context = {
        "request": request
    }
    accomodation = DailyAccomodation.objects.filter(day=day)
    accomodations = DailyAccomodationSerializer(accomodation, many=True, context=context).data

    if accomodations:
        return Response(accomodations, status=200)
    else:
        return Response({"detail": "No services"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_dailydestinations(request):
    day = request.query_params.get("id")

    context = {
        "request": request
    }
    destination = DailyDestination.objects.filter(day=day)
    destinations = DailyDestinationSerializer(destination, many=True, context=context).data

    if destinations:
        return Response(destinations, status=200)
    else:
        return Response({"detail": "No services"}, status=400)

@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_day(request, pk):
    day = Day.objects.filter(id=pk).first()
    day_data = DaySerializer(instance=day, data=request.data)

    if day_data.is_valid():
        day_data.save()
        return Response({"detail": "Day updated"}, status=200)
    return Response({"detail": "Day not updated"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_accomodation(request):
    acc_id = request.query_params.get("id")

    context = {
        "request": request
    }
    accomodation = Accomodation.objects.get(id=acc_id)
    accomodation_data = AccomodationSerializer(accomodation, context=context).data

    if accomodation_data:
        return Response(accomodation_data, status=200)
    else:
        return Response({"detail": "Acomodation does not exist"}, status=400)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def get_destination(request):
    des_id = request.query_params.get("id")

    context = {
        "request": request
    }
    destination = Destination.objects.get(id=des_id)
    destination_data = DestinationSerializer(destination, context=context).data

    if destination_data:
        return Response(destination_data, status=200)
    else:
        return Response({"detail": "Acomodation does not exist"}, status=400)

@api_view(['POST'])
# @permission_classes([IsAuthenticated])
def get_report(request):
    company_id = request.data.get("company")
    start_date = request.data.get("startDate")
    end_date = request.data.get("endDate")
    total_cost = 0
    total_profit = 0
    total_income = 0
    total_people = 0
    context = {
        "request": request
    }
    data = {}

    company = Company.objects.get(id=company_id)

    if company:
        itineraries = Itinerary.objects.filter(company=company_id, created_at__range=[start_date, end_date])
        itinerary_data = ItinerarySerializer(itineraries, many=True, context=context).data

        for itinerary in itineraries:
            total_cost = total_cost + int(itinerary.serializable_value('cost'))
            total_profit = total_profit + int(itinerary.serializable_value('profit'))
            total_income = total_income + int(itinerary.serializable_value('total_cost'))
            total_people = total_people + int(itinerary.serializable_value('people'))

        data["itineraries"] = itinerary_data
        data["total_costs"] = total_cost
        data["total_profit"] = total_profit
        data["total_income"] = total_income
        data["total_people"] = total_people

        return Response(data, status=200)

    return Response({"detail": "Invalid Company"}, status=400)



@api_view(['PATCH'])
# @permission_classes([IsAuthenticated])
def update_costs(request, pk):
    Itinerary.objects.filter(id=pk).update(cost=request.data['cost'], profit=request.data['profit'], total_cost=request.data['total_cost'])
    return Response({"detail": "data updated"}, status=200)
