from rest_framework import serializers
from .models import Company, Accomodation, Destination, Contact, Itinerary, Country, Service, Day, DailyAccomodation, DailyDestination, Includes, Excludes

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'email', 'phone', 'permission', 'username', 'password', 'logo', 'website', 'permission')

class AccomodationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Accomodation
        fields = ('id', 'name', 'email', 'address', 'picture', 'description')

class DestinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Destination
        fields = ('id','name', 'location', 'picture', 'description')

class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('id', 'company', 'name', 'phone', 'email')

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'name', 'currency', 'weather', 'food', 'culture')


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('id', 'company', 'service', 'price')

class ItinerarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Itinerary
        fields = ('id', 'company', 'country', 'contact', 'title', 'client', 'people', 'markup', 'email', 'cost', 'profit', 'total_cost', 'created_at')

class DaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Day
        fields = ('id', 'itinerary', 'number', 'title', 'summary', 'date')

class DailyAccomodationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyAccomodation
        fields = ('id', 'day', 'service', 'name', 'board', 'room', 'nights', 'checkin', 'checkout')

class DailyDestinationSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyDestination
        fields = ('id', 'day', 'service', 'name', 'location', 'distance', 'duration', 'pickup')

class IncludesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Includes
        fields = ('id', 'itinerary', 'service', 'description')

class ExcludesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Excludes
        fields = ('id', 'itinerary', 'description')

