# Generated by Django 3.1.5 on 2021-03-04 08:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tuasoft', '0003_auto_20210304_0859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='itinerary',
            name='email',
            field=models.EmailField(blank=True, max_length=100, null=True, unique=True),
        ),
    ]
