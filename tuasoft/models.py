from django.db import models
import uuid
from datetime import datetime
from django.db.models import JSONField



class Company(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    phone = models.CharField(max_length=100)
    logo = models.CharField(max_length=500)
    website = models.CharField(max_length=100)
    permission = models.CharField(max_length=100)

class Accomodation(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True, null=True, blank=True)
    address = models.CharField(max_length=100)
    picture = models.CharField(max_length=500)
    description = models.CharField(max_length=2000, unique=True, null=True, blank=True)

class Destination(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    picture = models.CharField(max_length=400)
    description = models.CharField(max_length=2000, unique=True, null=True, blank=True)

class Country(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100)
    currency = models.CharField(max_length=1000)
    weather = models.CharField(max_length=1000)
    food = models.CharField(max_length=1000)
    culture = models.CharField(max_length=1000)

class Contact(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=1000)
    email = models.EmailField(max_length=100, unique=True)

class Itinerary(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    client = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, null=True, blank=True, unique=True)
    people = models.IntegerField()
    markup = models.IntegerField()
    cost = models.IntegerField(null=True)
    profit = models.IntegerField(null=True)
    total_cost = models.IntegerField(null=True)
    created_at = models.DateField(auto_now=True)

class Day(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    itinerary = models.ForeignKey(Itinerary, on_delete=models.CASCADE)
    number = models.IntegerField()
    title = models.CharField(max_length=1000)
    summary = models.CharField(max_length=3000)
    date = models.DateField()

class Service(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null= True)
    service = models.CharField(max_length=100)
    price = models.IntegerField()


class DailyAccomodation(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    board = models.CharField(max_length=1000)
    room = models.CharField(max_length=100)
    nights = models.IntegerField()
    checkin = models.DateField()
    checkout = models.DateField()

class DailyDestination(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    day = models.ForeignKey(Day, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=1000)
    distance = models.IntegerField()
    duration = models.IntegerField()
    pickup = models.DateField()

class Includes(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    itinerary = models.ForeignKey(Itinerary, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)

class Excludes(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    itinerary = models.ForeignKey(Itinerary, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)

