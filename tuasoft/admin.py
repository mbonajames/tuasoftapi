from django.contrib import admin
from .models import Company, Accomodation, Destination, Contact, Itinerary, Country, Service, Day, DailyAccomodation, DailyDestination, Includes, Excludes

admin.site.register(Company)
admin.site.register(Accomodation)
admin.site.register(Destination)
admin.site.register(Contact)
admin.site.register(Itinerary)
admin.site.register(Country)
admin.site.register(Service)
admin.site.register(Day)
admin.site.register(DailyAccomodation)
admin.site.register(DailyDestination)
admin.site.register(Includes)
admin.site.register(Excludes)