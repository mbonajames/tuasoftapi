
from django.contrib import admin
from django.urls import path, include
from .router import router
from tuasoft import views

urlpatterns = [

    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/login', views.login, name='login'),
    path('api/contact', views.get_contact, name='contact'),
    # path('api/daily-report', views.get_client_number, name='daily-report'),
    path('api/add-itinerary', views.add_itinerary, name='add-itinerary'),
    path('api/add-include', views.add_include, name='add-include'),
    path('api/add-exclude', views.add_exclude, name='add-exclude'),
    path('api/add-day', views.add_day, name='add-day'),
    path('api/add-daily-accomodation', views.add_daily_accomodation, name='add-daily-accomodation'),
    path('api/add-daily-destination', views.add_daily_destination, name='add-daily-destination'),
    path('api/get-itinerary', views.get_itinerary, name='get-itinerary'),
    path('api/get-company', views.get_company, name='get-company'),
    path('api/get-country', views.get_country, name='get-country'),
    path('api/get-itineraries', views.get_itineraries, name='get-itineraries'),
    path('api/update-daily-accomodation/<str:pk>', views.update_accomodation, name='update-daily-accomodation'),
    path('api/update-daily-destination/<str:pk>', views.update_Destination, name='update-daily-destination'),
    path('api/delete', views.delete, name='delete'),
    path('api/signup', views.signup, name='signup'),
    path('api/update-contact/<str:pk>', views.update_contact, name='update-contact'),
    path('api/add-service', views.add_service, name='add-service'),
    path('api/update-service/<str:pk>', views.update_service, name='update-service'),
    path('api/add-contact', views.add_contact, name='add-contact'),
    path('api/update-company/<str:pk>', views.update_company, name='update-company'),
    path('api/get-company', views.get_company, name='get-company'),
    path('api/get-services', views.get_services, name='get-services'),
    path('api/get-includes', views.get_includes, name='get-includes'),
    path('api/get-excludes', views.get_excludes, name='get-excludes'),
    path('api/get-daily-accomodations', views.get_dailyaccomodations, name='get-daily-accomodations'),
    path('api/get-daily-destinations', views.get_dailydestinations, name='get-daily-destinations'),
    path('api/update-day/<str:pk>', views.update_day, name='update-day'),
    path('api/get-accomodation', views.get_accomodation, name='get-accomodation'),
    path('api/get-destination', views.get_destination, name='get-destination'),
    path('api/get-report', views.get_report, name='get-report'),
    path('api/update-costs/<str:pk>', views.update_costs, name='update-costs'),
]
