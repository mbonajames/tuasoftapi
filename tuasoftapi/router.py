from tuasoft.views import CompanyViewSet, CountryViewSet, ServiceViewSet, AccomodationViewSet, DestinationViewSet
from rest_framework import routers
from django.urls import path

router = routers.DefaultRouter()
router.register('company', CompanyViewSet)
router.register('country', CountryViewSet)
router.register('services', ServiceViewSet)
router.register('accomodations', AccomodationViewSet)
router.register('destinations', DestinationViewSet)




